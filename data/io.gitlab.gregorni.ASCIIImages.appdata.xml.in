<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
	<id>io.gitlab.gregorni.ASCIIImages.desktop</id>
	<metadata_license>CC0-1.0</metadata_license>
	<project_license>GPL-3.0-or-later</project_license>
  
  <name>Letterpress</name>
  <developer_name>Letterpress Contributors</developer_name>
  <summary>Create beautiful ASCII art</summary>
  
	<description>
	  <p>
	    Letterpress converts your images into a picture made up of ASCII characters.
	    You can save the output to a file, copy it, and even change its resolution!
	    High-res output can still be viewed comfortably by lowering the zoom factor.
	  </p>
	</description>
  
  <categories>
    <category>GTK</category>
    <category>GNOME</category>
    <category>TextTools</category>
    <category>Graphics</category>
    <category>ImageProcessing</category>
  </categories>
  
  <keywords>
    <keyword>image</keyword>
    <keyword>ascii</keyword>
    <keyword>convert</keyword>
    <keyword>conversion</keyword>
    <keyword>text</keyword>
  </keywords>
  
  <requires>
    <internet>offline-only</internet>
    <display_length compare="ge">360</display_length>
  </requires>
  
  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </recommends>
  
  <screenshots>
    <screenshot>
      <image>https://gitlab.com/gregorni/Letterpress/-/raw/main/data/screenshots/overview.png</image>
      <caption>Overview</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/gregorni/Letterpress/-/raw/main/data/screenshots/copied.png</image>
      <caption>Copy output</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/gregorni/Letterpress/-/raw/main/data/screenshots/saved.png</image>
      <caption>Save output</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/gregorni/Letterpress/-/raw/main/data/screenshots/width.png</image>
      <caption>Adjust output width</caption>
    </screenshot>
  </screenshots>
  
  <launchable type="desktop-id">io.gitlab.gregorni.ASCIIImages.desktop</launchable>
  
  <url type="homepage">https://gitlab.com/gregorni/Letterpress</url>
  <url type="bugtracker">https://gitlab.com/gregorni/Letterpress/-/issues</url>
  <url type="help">https://gitlab.com/gregorni/Letterpress/-/issues</url>
  <url type="vcs-browser">https://gitlab.com/gregorni/Letterpress</url>
  <url type="contribute">https://gitlab.com/gregorni/Letterpress</url>
  <url type="contact">https://matrix.to/#/#gregorni-apps:matrix.org</url>
  
  <update_contact>gregorniehl@web.de</update_contact>
  
  <content_rating type="oars-1.1" />
  
  <project_group>GNOME</project_group>
  
  <releases>
    <release version="1.3.0" date="2023-05-07">
      <description>
        <ul>
          <li>Rebrand to Letterpress</li>
          <li>Replaced icon on welcome screen with illustration</li>
          <li>Changed warning dialog to Adw.MessageDialog</li>
          <li>Improved adaptiveness of "Output Width" subtitle</li>
          <li>Swapped buttons for copying and saving output</li>
          <li>Added a string to translations</li>
          <li>Added Turkish translation</li>
          <li>Added Czech translation</li>
        </ul>
      </description>
    </release>
    <release version="1.2.0" date="2023-04-10">
      <description>
        <ul>
          <li>Added French translation</li>
          <li>Added Russian translation</li>
          <li>Added Occitan translation</li>
          <li>Added Italian translation</li>
          <li>Fixed "Open with…" functionality</li>
          <li>The app now remembers window size and state</li>
        </ul>
      </description>
    </release>
    <release version="1.0.1" date="2023-03-25">
      <description>
        <p>First Release! 🎉</p>
      </description>
    </release>
  </releases>
  
  <kudos>
      <!--
        GNOME Software kudos:
        https://gitlab.gnome.org/GNOME/gnome-software/blob/master/doc/kudos.md
      -->
      <kudo>ModernToolkit</kudo>
      <kudo>HiDpiIcon</kudo>
  </kudos>

  <custom>
    <value key="Purism::form_factor">workstation</value>
    <value key="Purism::form_factor">mobile</value>
  </custom>

</component>

